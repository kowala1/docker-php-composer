FROM php:8.1-cli

RUN apt-get update &&  \
    apt-get install -y unzip

RUN curl https://getcomposer.org/download/latest-2.x/composer.phar -o /usr/bin/composer && \
    chmod a+x /usr/bin/composer

COPY docker-entrypoint.sh /docker-entrypoint.sh

WORKDIR /app

ENTRYPOINT [ "/docker-entrypoint.sh" ]

CMD ["composer"]